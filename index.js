var express = require("express");
var app = express();

app.get("/", (req, resp)=>{
    resp.send("It worked");
});

app.get("/more", (req, resp)=>{
  resp.send("More Stuff")
});

const port = process.env.PORT || 3000;
app.listen(port)
console.log("Listening on port " + port);
